MarsComm
===

This is an Instant Messaging App which simulates that the sender is in Earth and the recipient user is in Mars or viceversa. 

The system takes into account the current real distance between Earth and Mars delaying the message arrival just like a real message would be travelling close to the speed of light.

